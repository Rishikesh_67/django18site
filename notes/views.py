from django.shortcuts import render

def hello(request):
   return render(request, "myapp/template/hello.html", {})
Views can also accept parameters −

from django.http import HttpResponse

def hello(request, number):
   text = "<h1>welcome to my app number %s!</h1>"% number
   return HttpResponse(text)

   
#When linked to a URL, the page will display the number passed as a parameter. Note that the parameters will be passed via the URL (discussed in the next chapter).

#Custom form in view
def login2(request): #Simple ordinary form
	form=LoginForm(request.POST or None)
	if form.is_valid():
		print form.cleaned_data
		for k,v in form.cleaned_data.iteritems(): ##request.cleaned_data will not work here...WSGIRequest' object has no attribute 'cleaned_data'
			print k," => ",v
		to_email=form.cleaned_data.get("email")#request.cleaned_data will not work here...WSGIRequest' object has no attribute 'cleaned_data'
		print "Sending an email to ",to_email
		send_mail("Simple Description",
			"A simple message",
			settings.EMAIL_HOST_USER,
			[to_email,],
			html_message="""
			<h1 style='color:blue;'>Hello Golangers</h1>
			""",
			fail_silently=True,
			)
		print "email sent..."
	return render(request,"login2.html",{"form":form}) 