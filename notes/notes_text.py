from .models import SignUp

import re

class SignUpForm(forms.ModelForm):
	class Meta:
		model=SignUp
		# form = SignUp
		fields=["username","email"] #Some fields can be excluded

	def clean_username(self):
		signup_dict=self.cleaned_data
		print "SignUp Details : ",signup_dict
		username=signup_dict["username"]
		lst=re.findall(r"^([A-Z]{1})([a-z]{2,19})$",username)
		print "Matched : ",lst
		if not len(lst)==1:
			print "Not matched..."
			raise forms.ValidationError("First letter of username should be in capital followed by 2 to 19 small case letters")
		return username

	def clean_email(self):
		signup_dict=self.cleaned_data
		print "SignUp details : ",signup_dict
		email=signup_dict["email"]
		lst=re.findall(r"^[a-z]{3,30}@gmail.com$",email)
		print "Matched : ",lst
		if not len(lst)==1:
			raise forms.ValidationError("Only gmail is allowed. Email should be in the form eg. hem@gmail.com & golang@gmail.com etc.Maximum length should be 30")
		return email #In absence of this line => This field can't be null
"""
View & Template Context:-
"""
....
	def home(request):
	title="HyGo"
	logout=""
	login="Login"
	# username="Dear visitor"    """............Won't affect"""
	username=""

	if request.method=="POST":
		print request.POST

	if request.user.is_authenticated():
		username=request.user
		logout="Logout"
		login=""
	else:
		request.user="Dear visitor"
	context={"title":title,"username":username,"logout":logout,"login":login}
	return render(request,"home.html",context)

"""
Form in view
"""
...
forms.py
...
class LoginForm(forms.Form):
	username=forms.CharField(max_length=20)
	email=forms.EmailField(max_length=30)

	def clean_username(self):
		signup_dict=self.cleaned_data
		print "Login Details : ",signup_dict
		username=signup_dict["username"]
		lst=re.findall(r"^([A-Z]{1})([a-z]{2,19})$",username)
		print "Matched : ",lst
		if not len(lst)==1:
			print "Not matched..."
			raise forms.ValidationError("First letter of username should be in capital followed by 2 to 19 small case letters")
		return username
...
views.py
...
def login2(request): #Simple ordinary form
	form=LoginForm(request.POST or None)
	if form.is_valid():
		print form.cleaned_data
		for k,v in form.cleaned_data.iteritems(): ##request.cleaned_data will not work here...WSGIRequest' object has no attribute 'cleaned_data'
			print k," => ",v
		to_email=form.cleaned_data.get("email")#request.cleaned_data will not work here...WSGIRequest' object has no attribute 'cleaned_data'
		print "Sending an email to ",to_email
		send_mail("Simple Description",
			"A simple message",
			settings.EMAIL_HOST_USER,
			[to_email,],
			html_message="""
			<h1 style='color:blue;'>Hello Golangers</h1>
			""",
			fail_silently=True,
			)
		print "email sent..."
	return render(request,"login2.html",{"form":form}) 


...

  <div class="col-sm-offset-4 col-sm-10">
        <form class="form" method="POST" action="">{% csrf_token %}<!--In case of action included-->
            {{ form.as_p }}  <!--{{ form }}-->
            <button type="submit" class="btn btn-default">Login</button>
        </form>
        

      </div>  
...

